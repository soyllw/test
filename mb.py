from ma import Second


class B(Second):
    def __init__(self, *args, **kwargs):
        super(B, self).__init__(*args, **kwargs)

    def fnc(self, number_one, number_two):
        return number_one * number_two * self.i
