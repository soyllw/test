class Parent(object):

    def __init__(self, i=3):
        self.__i = i

    def fnc(self, num):
        return num * num * self.i

    def isFirst(self):
        return 0

    @property
    def isSecond(self):
        return 0

    @property
    def i(self):
        return self.__i

    @i.setter
    def i(self, value):
        self.__i = value


class First(Parent):

    def isFirst(self):
        return 1


class Second(Parent):

    @property
    def isSecond(self):
        return 1


class A(First):

    def fnc(self, num):
        if num == 7:
            raise MyError("Error text")
        else:
            return super(A, self).fnc(num)


class MyError(Exception):
    pass
